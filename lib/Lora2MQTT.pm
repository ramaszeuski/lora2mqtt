package Lora2MQTT;

use strict;
use warnings;

use Module::Pluggable search_path => [
    __PACKAGE__ . '::Parser',
], require => 1;

sub new {
    my $classname = shift;
    my $args      = shift;

    my $self = {};
    bless ( $self, $classname );

    my @plugins = $self->plugins;

    if ( $args->{formats} ) {
        $self->{formats} = parse_formats( $args->{formats} );
    }

	return $self;
}

sub parse_formats {
    my $source  = shift;
    my $formats = {};

    VENDOR:
    foreach my $vendor ( keys %{ $source } ) {
        CLASS:
        foreach my $class ( keys %{ $source->{ $vendor } } ) {

            my @unpack = ();
            my @fields = ();

            ITEM:
            foreach my $item ( split /\s+/, $source->{ $vendor }{ $class }) {
                my ( $field, $size, $multiplier, $offset ) = split /:/, $item;

                my $signed = ( $size =~ s/^\+// ) ? 1 : 0;
                push @unpack,  ('', 'C', 'n', '', 'N')[ $size ];
                push @fields, {
                    name       => $field,
                    multiplier => $multiplier // 1,
                    offset     => $offset // 0,
                    signed     => $signed,
                    size       => $size,
                };
            }
            $formats->{ $vendor .'-'. $class } = {
                unpack => join(' ', @unpack),
                fields => \@fields,
            }
        }
    }
    return $formats;
}

sub parse_payload {
    my $self    = shift;
    my $sensor  = shift;
    my $payload = shift;

    return { raw => $payload } if $payload !~ /^[\da-f]+$/;

    if ( my $format = $self->{formats}{ $sensor->{vendor} . '-' . $sensor->{class} } ) {
        my $rc   = {};
        my @data = unpack( $format->{unpack}, pack('H*', $payload ));
        FIELD:
        foreach my $field ( @{ $format->{fields}} ) {
            my $data = shift @data;
            next FIELD if ! $field->{name};
            $rc->{ $field->{name} } = $data / $field->{multiplier} - $field->{offset};
        }

        return $rc;
    }
    elsif ( my $parser = join('::', __PACKAGE__, 'Parser', $sensor->{vendor}, $sensor->{class}) ) {
        if ( $parser->can('parse') ) {
            return $parser->parse( pack('H*', $payload ) );
        }
    }


    return { hex => $payload };
}

1;

__END__

#    elsif ( $class eq 'custom-gps' && $payload =~ /(.{6})(.{6})/i ) {
#        return {
#            lat => hex($1) / 16777215.0 * 180 - 90,
#            lon => hex($2) / 16777215.0 * 360 - 180,
#        };
#    }
#    elsif ( $class eq 'logarex-energy' && $payload =~ /21(.{8})(.{8})/i ) {
#        return {
#            '+a' => hex($1) / 100,
#            '-a' => hex($2) / 100,
#        };
#    }
#    elsif ( $class eq 'gospace-parking' && $payload =~ /(..)(..)(..)(..)(....)/i ) {
#        return {
#            event            => hex($1),
#            temperature      => hex($4),
#            battery          => hex($5) / 1000,
#            #mag x y z
#        };
#    }

