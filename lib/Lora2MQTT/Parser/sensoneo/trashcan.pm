package Lora2MQTT::Parser::sensoneo::trashcan;

sub parse {
    my $self = shift;
    my $data = shift;

    $data = join('', unpack('a*', $data));

    if ( $data =~ /U([\d\.]+)T([\+\-]?\d+)D(\d+)P(\d)/) {
        return {
            battery     => $1 * 1,
            temperature => $2 * 1,
            distance    => $3 * 1,
            position    => $4,
        };
    }
    else {
        return {
            ascii => $data,
        };
    }
}

1;

