package Lora2MQTT::Parser::maxify::trashcan;

sub parse {
    my $self = shift;
    my $data = shift;

    my @data = split(',', join('', unpack('a*', $data)));

    return {
         battery  => $data[8] / 1000,
         alarm    => $data[9],
         distance => $data[7] * 1,
    };
}

1;
